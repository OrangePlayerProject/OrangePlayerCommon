package org.orangeplayer.common.interfaces;

public interface Communicable extends Transmissible, Receivable {}
