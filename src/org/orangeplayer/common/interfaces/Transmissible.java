package org.orangeplayer.common.interfaces;

import java.io.IOException;

public interface Transmissible {
    public void send(byte[] data) throws IOException;
    public void send(Object obj) throws IOException;
    public void send(String str) throws IOException;
    public void send(int data) throws IOException;
    public void send(byte data) throws IOException;
}
