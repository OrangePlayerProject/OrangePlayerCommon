package org.orangeplayer.common.ontesting;

import org.orangeplayer.common.streams.encryptor.DataEncryptor;
import org.orangeplayer.common.streams.encryptor.exceptions.InvalidTextException;

import java.util.Arrays;

public class TestEncryptor {
    public static void main(String[] args) throws InvalidTextException {
        DataEncryptor enc = new DataEncryptor();
        byte[] txt = "holaxd".getBytes();
        byte[] e = enc.encrypt(txt);
        byte[] d = enc.decrypt(e);
        System.out.println(Arrays.toString(txt));
        System.out.println(Arrays.toString(e));
        System.out.println(Arrays.toString(d));
    }
}
