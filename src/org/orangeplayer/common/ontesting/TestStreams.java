package org.orangeplayer.common.ontesting;

import org.orangeplayer.common.streams.OrangeStream;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class TestStreams {
    public static void main(String[] args) throws IOException {
        int port = 4000;
        int socketSize = (int) Math.pow(1024, 2);
        int bufferSize = (int) (Math.pow(1024, 1));
        byte[] buffer = new byte[bufferSize];
        Random rand = new Random();

        for (int i = 0; i < buffer.length; i++)
            buffer[i] = (byte) rand.nextInt(256);

        ServerSocket serverSocket = new ServerSocket(port);
        Socket client = new Socket("localhost", port);
        Socket sockServ = serverSocket.accept();
        sockServ.setSendBufferSize(socketSize);
        sockServ.setReceiveBufferSize(socketSize);
        client.setReceiveBufferSize(socketSize);
        client.setSendBufferSize(socketSize);

        OrangeStream stream = new OrangeStream(client.getOutputStream(), sockServ.getInputStream());

        new Thread(()->{
            System.out.println("Receiving data....");
            try {
                printEquals(buffer, stream.readBytes());
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Data received");
        }).start();

        System.out.println("Sending data....");
        stream.getOutputStream().writeBytes(buffer);
    }

    public static boolean isEqualsStreams(byte[] a, byte[] b) {
        if (a.length != b.length)
            return false;

        boolean isEquals = true;

        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                isEquals = false;
                break;
            }
        }

        return isEquals;


    }

    public static void printEquals(byte[] a, byte[] b) {
        boolean isEquals = isEqualsStreams(a, b);
        System.err.println(isEquals?"Son Iguales" : "Son Distintos");
    }

}
