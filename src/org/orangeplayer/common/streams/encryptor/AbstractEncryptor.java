/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.orangeplayer.common.streams.encryptor;

import org.bytebuffer.ByteBuffer;
import org.orangeplayer.common.streams.encryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public abstract class AbstractEncryptor {
    protected ByteBuffer dataBuffer;

    public AbstractEncryptor() {
        dataBuffer = new ByteBuffer();
    }

    public abstract byte encryptByte(byte b);
    public abstract byte decryptByte(byte b);
    public abstract byte[] encrypt(byte[] data) throws InvalidTextException;
    public abstract byte[] decrypt(byte[] data) throws InvalidTextException;
}
