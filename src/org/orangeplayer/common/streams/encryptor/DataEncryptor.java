package org.orangeplayer.common.streams.encryptor;

public class DataEncryptor extends AbstractEncryptor {

    protected static final byte MULTIPLIER = -1;
    protected static final byte ADDER = 5;
    protected static final byte NULL = 20;

    public DataEncryptor() {
        super();
    }

    @Override
    public byte encryptByte(byte b){
        return (byte) (b-5);
    }

    @Override
    public byte decryptByte(byte b){
        return (byte) (b+5);
    }

    @Override
    public synchronized byte[] encrypt(byte[] data) {
        for (int i = 0; i < data.length; i++)
            dataBuffer.add(encryptByte(data[i]));

        return dataBuffer.drain();
    }

    @Override
    public synchronized byte[] decrypt(byte[] data) {
        for (int i = 0; i < data.length; i++)
            dataBuffer.add(decryptByte(data[i]));

        return dataBuffer.drain();
    }
}
