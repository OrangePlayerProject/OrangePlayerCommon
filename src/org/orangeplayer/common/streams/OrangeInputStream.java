/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.orangeplayer.common.streams;

import org.orangeplayer.common.streams.encryptor.DataEncryptor;
import org.orangeplayer.common.streams.interfaces.ReceivableStream;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.orangeplayer.common.streams.StreamParam.HASPKG;
import static org.orangeplayer.common.streams.StreamParam.HASRES;


/**
 *
 * @author martin
 */
public class OrangeInputStream extends DataInputStream implements ReceivableStream {

    private DataEncryptor encryptor;

    public OrangeInputStream(InputStream socketStream) {
        super(socketStream);
        encryptor = new DataEncryptor();
    }

    protected void waitForData() throws IOException {
        System.err.println("Waiting for data!");
        while (available() == 0){
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean hasPackages() throws IOException {
        return readShort() == HASPKG;
    }

    protected boolean hasResidue() throws IOException {
        return readShort() == HASRES;
    }

    protected byte[] readData() throws IOException {
        waitForData();
        int available = readInt();
        System.err.println("ReadDataAvailable: "+available);
        byte[] buffer = new byte[available];
        for (int i = 0; i < available; i++)
            buffer[i] = (byte) read();
        //System.out.println("Recd Data: "+new String(buffer));

        return encryptor.decrypt(buffer);
    }

    @Override
    public String readString() throws IOException {
        return new String(readData());
    }

    @Override
    public byte[] readBytes() throws IOException {
        return readData();
    }

    @Override
    public Object readObject() throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(readData());
        OIS ois = new OIS(bais);
        Object obj = ois.readObject();
        ois.close();
        bais.close();
        return obj;
    }

}
