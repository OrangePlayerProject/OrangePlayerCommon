package org.orangeplayer.common.streams;

import org.orangeplayer.common.streams.interfaces.StreamCommunicable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class OrangeStream implements StreamCommunicable {
    private final OrangeOutputStream outputStream;
    private final OrangeInputStream inputStream;

    public OrangeStream(OrangeOutputStream outputStream, OrangeInputStream inputStream) {
        this.outputStream = outputStream;
        this.inputStream = inputStream;
    }

    public OrangeStream(OutputStream outputStream, InputStream inputStream) {
        this(new OrangeOutputStream(outputStream), new OrangeInputStream(inputStream));
    }

    public OrangeOutputStream getOutputStream() {
        return outputStream;
    }

    public OrangeInputStream getInputStream() {
        return inputStream;
    }

    @Override
    public String readString() throws IOException {
        return inputStream.readString();
    }

    @Override
    public byte[] readBytes() throws IOException {
        return inputStream.readBytes();
    }

    @Override
    public Object readObject() throws IOException, ClassNotFoundException {
        return inputStream.readObject();
    }

    @Override
    public void writeString(String str) throws IOException {
        outputStream.writeString(str);
    }

    @Override
    public void writeBytes(byte[] data) throws IOException {
        outputStream.writeBytes(data);
    }

    @Override
    public void writeObject(Object obj) throws IOException {
        outputStream.writeObject(obj);
    }

}
