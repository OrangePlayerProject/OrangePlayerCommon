package org.orangeplayer.common.streams.interfaces;

import java.io.IOException;

public interface TransmissibleStream {
    public void writeString(final String str) throws IOException;

    public void writeBytes(final byte[] data) throws IOException;

    public void writeObject(Object obj) throws IOException;

}
