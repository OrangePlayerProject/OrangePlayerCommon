package org.orangeplayer.common.streams.interfaces;

public interface StreamCommunicable
        extends TransmissibleStream, ReceivableStream {}
