package org.orangeplayer.common.streams.interfaces;

import java.io.IOException;

public interface ReceivableStream {
    public String readString() throws IOException;

    public byte[] readBytes() throws IOException;

    public Object readObject() throws IOException, ClassNotFoundException;

}
