package org.orangeplayer.common.sys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class PropertiesManager {
    protected File infoFile;
    protected Properties props;

    protected String propertiesTitle;

    public PropertiesManager() {}

    public PropertiesManager(File infoFile, String propertiesTitle) {
        this.infoFile = infoFile;
        props = new Properties();
        this.propertiesTitle = propertiesTitle;
        File parent = infoFile.getParentFile();
        if (parent != null && !parent.exists())
            parent.mkdirs();
        if (infoFile.exists())
            loadData();
        else
            saveDefaultData();
    }

    public PropertiesManager(String infoFilePath, String propertiesTitle) {
        this(new File(infoFilePath), propertiesTitle);
    }

    public PropertiesManager(File parentFolder, String fileName, String title) {
        this(new File(parentFolder, fileName), title);
    }

    private void loadData() {
        try {
            props.load(new FileInputStream(infoFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveData() {
        try {
            props.store(new FileOutputStream(infoFile), propertiesTitle);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void restoreDefaultData() {
        props.clear();
        try {
            infoFile.createNewFile();
            props = new Properties();
            saveDefaultData();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract void saveDefaultData();

    public boolean isKey(String key, String valueFilter) {
        String value = props.getProperty(key);
        return value != null && value.equals(valueFilter);
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public int getNumericProperty(String key) {
        String strProps = getProperty(key);
        return strProps == null ? null : Integer.parseInt(strProps);
    }

    public void setProperty(String key, String newValue) {
        props.setProperty(key, newValue);
        saveData();
    }

    public void setProperty(String key, Number newValue) {
        props.setProperty(key, newValue.toString());
        saveData();
    }

    public void addProperty(String key, String value) {
        setProperty(key, value);
    }

    public void addProperty(String key, Number value) {
        setProperty(key, value.toString());
    }

}
