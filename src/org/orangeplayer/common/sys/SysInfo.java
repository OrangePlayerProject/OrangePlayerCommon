package org.orangeplayer.common.sys;

import java.io.File;

public class SysInfo {
    public static final File PARENTFOLDER;
    public static final File MUSICFOLDER;

    static {
        final String userName = System.getProperty("user.name");
        PARENTFOLDER = new File("/home/"+userName+"/", "OrangePlayer/");
        if (!PARENTFOLDER.exists())
            PARENTFOLDER.mkdirs();
        MUSICFOLDER = new File(PARENTFOLDER, "music");
        if (!MUSICFOLDER.exists())
            PARENTFOLDER.mkdir();
    }

}
