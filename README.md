# OrangePlayerCommon

## Version Description

## 0.1 RC
	- First commit
## 0.2 Beta
	- Se añade encriptador a los streams
	- Se crea clase OrangeStream para agrupar los streams de lectura y escritura en uno solo
	- Se crean interfaces TransmissibleStream, ReceivableStream y CommunicableStream para separar los metodos especiales creados en 
	interfaces aparte
## 0.2 RC
	- Se realiza test exitoso en la incorporacion del encriptador
